baseline
baseline: spec
	<baseline>
	spec
		for: #common
		do: [ self
				seaside: spec;
				bootstrap: spec;
				neoJSON: spec.
			spec
				package: 'SocialNetwork'
				with: [ spec requires: #('Seaside3' 'Bootstrap-Core' 'Bootstrap-Widgets' 'NeoJSON') ].
			spec postLoadDoIt: #postLoadActions ]