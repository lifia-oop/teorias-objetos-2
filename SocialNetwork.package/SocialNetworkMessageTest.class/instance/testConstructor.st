testing
testConstructor
	| before |
	before := DateAndTime now.
	message := SocialNetworkMessage from: john to: jane with: 'Hola Jane'.
	self assert: message from equals: john.
	self assert: message to equals: jane.
	self assert: message text equals: 'Hola Jane'.
	self assert: (message creationDateAndTime between: before and: DateAndTime now)