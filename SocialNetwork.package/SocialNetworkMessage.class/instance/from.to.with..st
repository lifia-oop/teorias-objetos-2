initialization
from: aPerson to: anotherPerson with: aString
	from := aPerson.
	to := anotherPerson.
	text := aString.
	creationDateAndTime := DateAndTime now