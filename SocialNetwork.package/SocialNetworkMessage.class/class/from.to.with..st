instance-creation
from: aPerson to: anotherPerson with: aString
	^ self new
		from: aPerson to: anotherPerson with: aString;
		yourself