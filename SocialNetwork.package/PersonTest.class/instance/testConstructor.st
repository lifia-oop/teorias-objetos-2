testing - access
testConstructor
	self assert: john follows isEmpty.
	self assert: john followers isEmpty.
	self assert: john sentMessages isEmpty.
	self assert: john receivedMessages isEmpty.
	self assert: john fullName equals: 'John Doe'.
	self assert: john email equals: 'john@doe.com'