testing - access
testSend
	| message |
	message := john send: 'Hola Jane' to: jane.
	self assert: message notNil.
	self assert: (john sentMessages includes: message).
	self assert: john receivedMessages isEmpty.
	self assert: jane sentMessages isEmpty.
	self assert: (jane receivedMessages includes: message)
	