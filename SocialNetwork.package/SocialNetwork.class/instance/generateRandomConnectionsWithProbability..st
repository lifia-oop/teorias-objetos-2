utility
generateRandomConnectionsWithProbability: probability
	"Build a random graph - connect two users with the given probability.
	 1 means everone will be connected to everyone else."
	| rand |
	rand := Random new.

	self users
		do: [ :first | 
			self users
				do: [ :second | 
					first ~= second
						ifTrue: [ rand next >= probability
								ifTrue: [ first follow: second ] ] ] ]