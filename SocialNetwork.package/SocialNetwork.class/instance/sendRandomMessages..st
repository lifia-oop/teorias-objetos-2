utility
sendRandomMessages: count
	"Sends count random messajes. Sender and receiver are chosen randomly. So is the text"

	| sender receiver text |
	count
		timesRepeat: [ sender := users atRandom.
			receiver := users atRandom.
			text := Object allSubclasses atRandom comment.
			sender send: text to: receiver ]