utility
createExample
	"Create and return a bunch of users and connects them randomly in aSocialNetwork "

	| network |
	network := self new.
	Person examples do: [ :each | network addUser: each ].
	network generateRandomConnectionsWithProbability: 0.6.
	network sendRandomMessages: 30.
	^ network