utility
example
	"I return an example instance for demonstration purpose. I always return the same instance."

	^ example ifNil: [ example := self createExample ]