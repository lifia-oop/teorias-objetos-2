messaging
send: aString to: aPerson
	| message |
	message := SocialNetworkMessage from: self to: aPerson with: aString.
	aPerson receive: message.
	self sentMessages add: message.
	^ message