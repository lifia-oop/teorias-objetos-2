examples
examples
	^ #('Sir Isaac Newton' 'Louis Pasteur ' 'Marie Curie' 'Albert Einstein' 'Charles Darwin' 'Nikola Tesla' 'James Clerk Maxwell' 'Michael Faraday ')
		collect: [ :each | Person fullName: each email: each asValidSelector, '@science.com' ]